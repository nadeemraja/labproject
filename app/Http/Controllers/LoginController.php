<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\myuser;
use Validator, Hash, DateTime, Mail, DB, AWS, PushNotification;

class LoginController extends Controller
{



    // login 
    public function login(request $request){
    
        $input = $request->all();
    
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        
        $email = $request->input('email');
        $password = $request->input('password');

        $user = myuser::where('email', '=', $email)->first();

        if (!$user) {
        //    return response()->json(['success'=>false, 'message' => 'Login Fail, please check email id']);
            return back()->with('error', 'Email-Address And Password Are Wrong.');
        }
        if (Hash::check($password, $user->password)) {
            // return response()->json(['success'=>true,'message'=>'success', 'data' => $user]);
            // return redirect('myusers/view');
            $myuser = $request->get('id',$user);  //->firstOrFail()
            return view('form.show', compact('myuser'));
        }else{
            // return response()->json(['success'=>false, 'message' => 'Login Fail, pls check password']);
            return back()->with('password', 'Login Fail, pls check password');
        }
            

        // if(auth()->attempt(array($fieldType => $input['username'], 'password' => $input['password'])))
        // {
        //     return redirect()->route('showUser');
        // }else{
        //     return redirect()->route('login')
        //         ->with('error','Email-Address And Password Are Wrong.');
        // }


    }


}
